/**
 * Copyright 2013 Twitter, Inc.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package com.sentimentos;

import org.json.JSONObject;
import com.google.common.collect.Lists;
import com.sentimentos.sentiwordnet.SentiWordNet;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

import cmu.arktweetnlp.Tagger.TaggedToken;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.JLabel;
import javax.swing.JProgressBar;

public class FilterStreamExample  implements Runnable {
	private static BufferedWriter buffWrite;
	private SentiWordNet sentiWordNet;
	private String term;
	private JProgressBar pbad, pgood;
	private JLabel txtTotal;
	private double good=0.0, bad=0.0, total =0.0;


	public FilterStreamExample(String term){
		sentiWordNet = new SentiWordNet();
		this.term = term;
		try {
			buffWrite = new BufferedWriter(new FileWriter("tweets.txt", true));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}


	public void twitter(String consumerKey, String consumerSecret, String token, 
			String secret) throws InterruptedException {
		BlockingQueue<String> queue = new LinkedBlockingQueue<String>(10000);
		StatusesFilterEndpoint endpoint = new StatusesFilterEndpoint();
		// add some track terms
		endpoint.trackTerms(Lists.newArrayList(term));

		Authentication auth = new OAuth1(consumerKey, consumerSecret, token, 
				secret);
		// Authentication auth = new BasicAuth(username, password);

		// Create a new BasicClient. By default gzip is enabled.
		Client client = new ClientBuilder()
				.hosts(Constants.STREAM_HOST)
				.endpoint(endpoint)
				.authentication(auth)
				.processor(new StringDelimitedProcessor(queue))
				.build();

		// Establish a connection
		client.connect();
		Tagger tagger = null;
		try {
			tagger = new Tagger("/cmu/arktweetnlp/model.20120919");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		List<TaggedToken> taggedTweet;
		// Do whatever needs to be done with messages
		for (int msgRead = 0; msgRead < 1000; msgRead++) {
			String msg = queue.take();
			String text = (new JSONObject(msg)).getString("text");

			if(!text.startsWith("RT")){
				try {
					escritor(text);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				taggedTweet = tagger.tokenizeAndTag(text);
				System.out.println(">>>  "+text);


				double totalTweet = sentiWordNet.classifica(taggedTweet);

				if(totalTweet>0){
					good+= 1;
					total += 1;
				}else if(totalTweet < 0) {
					bad+= 1;
					total += 1;
				}

				synchronized (pbad) {
					if(pbad != null){
						pbad.setValue((int)(bad*100/total));
					}
				}
				synchronized (pgood) {
					if(pgood != null){
						pgood.setValue((int)(good*100/total));
					}
				}
				synchronized (txtTotal) {
					if(txtTotal != null){
						txtTotal.setText(total+"");
					}
				}


			}
		}

		client.stop();
		try {
			buffWrite.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void txt(String filename) {

		Tagger tagger = null;
		try {
			tagger = new Tagger("/cmu/arktweetnlp/model.20120919");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		List<TaggedToken> taggedTweet;

		try { 
			FileReader arq = new FileReader(filename); 
			BufferedReader lerArq = new BufferedReader(arq); 
			String text = lerArq.readLine(); 
			while (text != null) { 
				taggedTweet = tagger.tokenizeAndTag(text);
				//System.out.println(">>>  "+text);
				sentiWordNet.classifica(taggedTweet);
				text = lerArq.readLine(); 
			}
			arq.close();
		} catch (IOException e) { 
			System.err.printf("Erro na abertura do arquivo: %s.\n", 
					e.getMessage());
		}
	}


	public JProgressBar getPbad() {
		return pbad;
	}


	public void setPbad(JProgressBar pbad) {
		this.pbad = pbad;
	}


	public JProgressBar getPgood() {
		return pgood;
	}


	public void setPgood(JProgressBar pgood) {
		this.pgood = pgood;
	}


	public static void escritor(String linha) throws IOException { 
		buffWrite.append(linha + "\n");
		buffWrite.flush(); 
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			twitter("WLkjypxgDkX1mKysXCKIOtBCy", 
					"YbmmcDNgojUFDuzKh4MW6YJ4vNJbrkDuLOf7JlEl6TGleVZdhL", 
					"4077729869-n0zQiTImlqRknEXeZbY9Drt1BlfHQI4sFTCzzWe", 
					"H8LZjCQo2c1LHB7mbhfOQlgUfUtVxYPjtxzddGAwF7ot0");
		} catch (InterruptedException e) {
			System.out.println(e);
		}
	}


	public JLabel getTxtTotal() {
		return txtTotal;
	}


	public void setTxtTotal(JLabel lblTotalDeTweets) {
		this.txtTotal = lblTotalDeTweets;
	}
}
