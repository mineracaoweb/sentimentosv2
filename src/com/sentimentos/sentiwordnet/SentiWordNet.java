package com.sentimentos.sentiwordnet;

import java.io.IOException;
import java.util.List;
import static com.google.common.collect.Lists.newArrayList;

import cmu.arktweetnlp.Tagger.TaggedToken;

public class SentiWordNet {
	private Library library;

	private List<String> badWords = newArrayList("bug", "bugs", "boring",
			"glitche","glitches", "shit", "WTF");

	private List<String> goodWords = newArrayList("loving", "love", "fun",
			"fantastic", "great");
	
	public SentiWordNet(){
		try {
			library = new Library("SentiWordNet.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public double classifica(List<TaggedToken> taggedTweet){
		double totalTweet = 0.0;
		double value;
		for (TaggedToken token1 : taggedTweet) {
			//removing the special characters from being classified
			if(!token1.tag.equals(",")){
				if(badWords.contains(token1.token)){
					value = -0.65;
				} else if (goodWords.contains(token1.token)){
					value = 0.65;
				} else{
					value = library.extract(token1.token, token1.tag.toLowerCase());
				}
				
				totalTweet += value;
			
				/**
				System.out.println(token1.token + ", " + 
						token1.tag.toLowerCase() + " "+ value);
				/**/
			}
		}
		if(totalTweet==0){
			System.out.println("2");
			//System.out.println("Neutro");
		}else if(totalTweet>0){
			System.out.println("1");
			//System.out.println("Positivo");
		}else {
			System.out.println("0");
			//System.out.println("Nagativo");
		}
		return totalTweet;
	}
}
