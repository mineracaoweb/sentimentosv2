package com.sentimentos.sentiwordnet;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class LerSaida {

	public static void main(String[] args) throws IOException {
		Library library = new Library("SentiWordNet.txt");

		String arquivo = "saida.txt";

		FileReader arq = new FileReader(arquivo); 
		BufferedReader lerArq = new BufferedReader(arq);
		String linha = lerArq.readLine();
		double total_linha = 0;
		int primeira_vez =1;
		
		while (linha != null) { 


			if (linha.substring(0,1).equals(">")){
				if(primeira_vez>1){ //checa se ja leu o tweet todo . Se sim ele vai checar qual foi o sentimento do tweet							
					if(total_linha==0){
						System.out.println("Esse tweet tem sentimento Neutro");
					}else if(total_linha>0){
						System.out.println("Esse tweet tem sentimento Positivo");
					}else {
						System.out.println("Esse tweet tem sentimento Negativo \n");
					}
					primeira_vez=1; // para nÃ£o entrar antes de nao ter terminado de contar os pesos
					total_linha =0; // zera para o proximo tweet
					
					
				}
				primeira_vez=2; // para poder contabilizar os pesos do tweet anterior

				System.out.println(linha); 
			}else {
				System.out.println("-----");
				System.out.println("palavra: "+linha.substring(2,linha.length()));
				System.out.println("classe gramatical: "+linha.substring(0,1).toLowerCase());
				System.out.println(linha +": "+library.extract(linha.substring(2,linha.length()), linha.substring(0,1).toLowerCase()));

				total_linha = total_linha +library.extract(linha.substring(2,linha.length()), linha.substring(0,1).toLowerCase());

			}

			linha = lerArq.readLine();
		}
		// para ver o total do ultimo tweet que nao pode ser impresso no loop
		if(total_linha==0){
			System.out.println("Esse tweet tem sentimento Neutro");
		}else if(total_linha>0){
			System.out.println("Esse tweet tem sentimento Positivo");
		}else {
			System.out.println("Esse tweet tem sentimento Negativo \n");
		}

		arq.close();
	}

}
