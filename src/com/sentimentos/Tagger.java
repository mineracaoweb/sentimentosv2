package com.sentimentos;

import cmu.arktweetnlp.Tagger.TaggedToken;

/*
 * Decompiled with CFR 0_110.
 * 
 * Could not load the following classes:
 *  cmu.arktweetnlp.Tagger$TaggedToken
 *  cmu.arktweetnlp.Twokenize
 *  cmu.arktweetnlp.impl.Model
 *  cmu.arktweetnlp.impl.ModelSentence
 *  cmu.arktweetnlp.impl.Sentence
 *  cmu.arktweetnlp.impl.Vocabulary
 *  cmu.arktweetnlp.impl.features.FeatureExtractor
 */

import cmu.arktweetnlp.Twokenize;
import cmu.arktweetnlp.impl.Model;
import cmu.arktweetnlp.impl.ModelSentence;
import cmu.arktweetnlp.impl.Sentence;
import cmu.arktweetnlp.impl.features.FeatureExtractor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Tagger {
    public Model model;
    public FeatureExtractor featureExtractor;

    public Tagger(String model) throws IOException{
    	loadModel(model);
    }
    
    public void loadModel(String modelFilename) throws IOException {
        this.model = Model.loadModelFromText((String)modelFilename);
        this.featureExtractor = new FeatureExtractor(this.model, false);
    }

    public List<TaggedToken> tokenizeAndTag(String text) {
        if (this.model == null) {
            throw new RuntimeException("Must loadModel() first before tagging anything");
        }
        List tokens = Twokenize.tokenizeRawTweetText((String)text);
        Sentence sentence = new Sentence();
        sentence.tokens = tokens;
        ModelSentence ms = new ModelSentence(sentence.T());
        this.featureExtractor.computeFeatures(sentence, ms);
        this.model.greedyDecode(ms, false);
        ArrayList<TaggedToken> taggedTokens = new ArrayList<TaggedToken>();
        for (int t = 0; t < sentence.T(); ++t) {
            TaggedToken tt = new TaggedToken();
            tt.token = (String)tokens.get(t);
            tt.tag = this.model.labelVocab.name(ms.labels[t]);
            taggedTokens.add(tt);
        }
        return taggedTokens;
    }
    
    
//
//    public static void main(String[] args) throws IOException {
//        if (args.length < 1) {
//            System.out.println("Supply the model filename as first argument.");
//        }
//        String modelFilename = args[0];
//        Tagger tagger = new Tagger();
//        tagger.loadModel(modelFilename);
//        String text = "RT @DjBlack_Pearl: wat muhfuckaz wearin 4 the lingerie party?????";
//        List<TaggedToken> taggedTokens = tagger.tokenizeAndTag(text);
//        for (TaggedToken token : taggedTokens) {
//            System.out.printf("%s\t%s\n", token.tag, token.token);
//        }
//    }
}