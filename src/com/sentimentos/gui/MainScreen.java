package com.sentimentos.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Toolkit;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import com.sentimentos.FilterStreamExample;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainScreen {

	private JFrame frmAnaliseDeSentimentos;
	private JTextField txtfallout;
	private JProgressBar progressBarGood;
	private JProgressBar progressBarBad;
	private Thread threadTweet;
	private JLabel lblTotalDeTweets;
	private JLabel lblNewtotal;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
					MainScreen window = new MainScreen();
					window.frmAnaliseDeSentimentos.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainScreen() {
		initialize();


	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAnaliseDeSentimentos = new JFrame();
		frmAnaliseDeSentimentos.setTitle("Analise de sentimentos");
		frmAnaliseDeSentimentos.setIconImage(Toolkit.getDefaultToolkit().getImage(MainScreen.class.getResource("/com/sun/java/swing/plaf/motif/icons/Warn.gif")));
		frmAnaliseDeSentimentos.setBounds(100, 100, 270, 348);
		frmAnaliseDeSentimentos.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAnaliseDeSentimentos.getContentPane().setLayout(null);

		txtfallout = new JTextField();
		txtfallout.setText("#Fallout4");
		txtfallout.setBounds(50, 11, 108, 20);
		frmAnaliseDeSentimentos.getContentPane().add(txtfallout);
		txtfallout.setColumns(10);

		JLabel lblTermo = new JLabel("Termo");
		lblTermo.setBounds(10, 14, 46, 14);
		frmAnaliseDeSentimentos.getContentPane().add(lblTermo);

		JButton btnIniciar = new JButton("Iniciar");
		btnIniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				FilterStreamExample fse = new FilterStreamExample(txtfallout.getText());
				fse.setPbad(progressBarBad);
				fse.setPgood(progressBarGood);
				fse.setTxtTotal(lblNewtotal);
				threadTweet = new Thread(fse);
				threadTweet.start();
			}
		});
		btnIniciar.setBounds(168, 10, 61, 23);
		frmAnaliseDeSentimentos.getContentPane().add(btnIniciar);

		progressBarGood = new JProgressBar();
		progressBarGood.setForeground(Color.GREEN);
		progressBarGood.setStringPainted(true);
		progressBarGood.setOrientation(SwingConstants.VERTICAL);
		progressBarGood.setBounds(21, 64, 81, 146);
		frmAnaliseDeSentimentos.getContentPane().add(progressBarGood);
		
		progressBarBad = new JProgressBar();
		progressBarBad.setForeground(Color.RED);
		progressBarBad.setStringPainted(true);
		progressBarBad.setOrientation(SwingConstants.VERTICAL);
		progressBarBad.setBounds(148, 64, 81, 146);
		frmAnaliseDeSentimentos.getContentPane().add(progressBarBad);

		JLabel lblGood = new JLabel("Good");
		lblGood.setBounds(50, 221, 46, 14);
		frmAnaliseDeSentimentos.getContentPane().add(lblGood);

		JLabel lblBad = new JLabel("Bad");
		lblBad.setBounds(183, 221, 46, 14);
		frmAnaliseDeSentimentos.getContentPane().add(lblBad);
		
		lblTotalDeTweets = new JLabel("Total de tweets:");
		lblTotalDeTweets.setBounds(21, 270, 81, 14);
		frmAnaliseDeSentimentos.getContentPane().add(lblTotalDeTweets);
		
		lblNewtotal = new JLabel("total");
		lblNewtotal.setBounds(137, 270, 46, 14);
		frmAnaliseDeSentimentos.getContentPane().add(lblNewtotal);
	}
}
